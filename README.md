<h1 align="center">🛒  Spring Boot project - Order Rest API 🛒</h1>
<h2 align="center"> 💻 Advanced Spring boot project using some functional programming concepts. This project also includes some concepts of BoundedContext 💻</h2>

### New features
* Customer order inquiry
* Order status change
* Custom error messages (Portuguese, English and Spanish)
* Inclusion of a message translation key for Portuguese, English and Spanish.
* Authentication application using JWT
* Application of unit tests with Mockito JUnit5 (in process)
* Swagger documentation APIs

- You can access the API at https://product-ordering-api-production.up.railway.app/swagger-ui.html

<h4 align="center"> 🚧 Project under construction 🚧</h4>
